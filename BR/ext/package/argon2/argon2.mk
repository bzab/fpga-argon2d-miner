################################################################################
#
# ARGON2
#
################################################################################

ARGON2_VERSION = 1.0
ARGON2_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/argon2
ARGON2_SITE_METHOD = local
#ARGON2_LICENSE = LGPLv2.1/GPLv2 
ARGON2_DEPENDENCIES += miner-module linux

define ARGON2_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) all -C $(@D)
endef
define ARGON2_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/argon2 $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
