################################################################################
#
# ARGON2FPGA
#
################################################################################

ARGON2FPGA_VERSION = 1.0
ARGON2FPGA_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/argon2fpga
ARGON2FPGA_SITE_METHOD = local
#ARGON2FPGA_LICENSE = LGPLv2.1/GPLv2 
CPUMINER_DEPENDENCIES += miner-module linux

define ARGON2FPGA_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) all -C $(@D)
endef
define ARGON2FPGA_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/argon2fpga $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
