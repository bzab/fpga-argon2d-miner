################################################################################
#
# ARGON2FPGANEON
#
################################################################################

ARGON2FPGANEON_VERSION = 1.0
ARGON2FPGANEON_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/argon2fpganeon
ARGON2FPGANEON_SITE_METHOD = local
#ARGON2FPGANEON_LICENSE = LGPLv2.1/GPLv2 
CPUMINER_DEPENDENCIES += miner-module linux

define ARGON2FPGANEON_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) all -C $(@D)
endef
define ARGON2FPGANEON_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/argon2fpganeon $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
