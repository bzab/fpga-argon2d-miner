################################################################################
#
# ARGON2NEON
#
################################################################################

ARGON2NEON_VERSION = 1.0
ARGON2NEON_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/argon2neon
ARGON2NEON_SITE_METHOD = local
#ARGON2NEON_LICENSE = LGPLv2.1/GPLv2 
CPUMINER_DEPENDENCIES += miner-module linux

define ARGON2NEON_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) all -C $(@D)
endef
define ARGON2NEON_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/argon2neon $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
