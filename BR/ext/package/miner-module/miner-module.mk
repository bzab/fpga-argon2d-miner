################################################################################
#
# MINER-module
#
################################################################################

MINER_MODULE_VERSION = 1.0
MINER_MODULE_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/miner
MINER_MODULE_SITE_METHOD = local
#MINER_MODULE_LICENSE = LGPLv2.1/GPLv2 

$(eval $(kernel-module))
$(eval $(generic-package))
