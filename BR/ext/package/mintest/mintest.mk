################################################################################
#
# MINTEST
#
################################################################################

MINTEST_VERSION = 1.0
MINTEST_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/mintest
MINTEST_SITE_METHOD = local
#MINTEST_LICENSE = LGPLv2.1/GPLv2 
MINTEST_DEPENDENCIES += miner-module linux

define MINTEST_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) mintest -C $(@D)
endef
define MINTEST_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/mintest $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
