################################################################################
#
# MINTEST2
#
################################################################################

MINTEST2_VERSION = 1.0
MINTEST2_SITE    = $(BR2_EXTERNAL_E2B_MINI_PATH)/src/mintest2
MINTEST2_SITE_METHOD = local
#MINTEST2_LICENSE = LGPLv2.1/GPLv2 
MINTEST2_DEPENDENCIES += miner-module linux

define MINTEST2_BUILD_CMDS
   $(MAKE) $(TARGET_CONFIGURE_OPTS) mintest2 -C $(@D)
endef
define MINTEST2_INSTALL_TARGET_CMDS 
   $(INSTALL) -D -m 0755 $(@D)/mintest2 $(TARGET_DIR)/usr/bin 
endef

$(eval $(generic-package))
