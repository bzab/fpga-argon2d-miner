#include<stdio.h>
#include<sys/types.h>
#include<stdint.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<sys/ioctl.h>
#include<inttypes.h>

#include "miner_ioc_cmds.h"

int plik;
volatile uint32_t * buf;

#define rotr64(x, y)  (((x) >> (y)) ^ ((x) << (64 - (y))))

#define G(a,b,c,d) \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 32); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 24); \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 16); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 63); 

#define BLAKE2_ROUND_NOMSG(v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15)  \
	G(v0, v4, v8, v12); \
	G(v1, v5, v9, v13); \
	G(v2, v6, v10, v14); \
	G(v3, v7, v11, v15); \
	G(v0, v5, v10, v15); \
	G(v1, v6, v11, v12); \
	G(v2, v7, v8, v13); \
	G(v3, v4, v9, v14); 


/*designed by the Lyra PHC team */
static inline uint64_t fBlaMka(uint64_t x, uint64_t y)
{
	uint32_t lessX = (uint32_t)x;
	uint32_t lessY = (uint32_t)y;

	uint64_t lessZ = (uint64_t)lessX;
	lessZ = lessZ * lessY;
	lessZ = lessZ << 1;

	uint64_t z = lessZ + x + y;

	return z;
}

void DumpHex(const void* data, size_t size) {
	char ascii[17];
	size_t i, j;
	ascii[16] = '\0';
	for (i = 0; i < size; ++i) {
		printf("%02X ", ((unsigned char*)data)[i]);
		if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			printf(" ");
			if ((i+1) % 16 == 0) {
				printf("|  %s \n", ascii);
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					printf(" ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					printf("   ");
				}
				printf("|  %s \n", ascii);
			}
		}
	}
	fflush(stdout);
}

uint32_t get_random32()
{
    uint32_t x;
    x = rand() & 0xff;
    x |= (rand() & 0xff) << 8;
    x |= (rand() & 0xff) << 16;
    x |= (rand() & 0xff) << 24;

    return x;
}

int main(int argc, char **argv)
{
  printf("I'm trying to open device!\n");
  fflush(stdout);
  plik=-1;
  plik=open("/dev/miner0", O_RDWR);
  if(plik==-1)
    {
      perror("/dev/miner0");
      printf("I can't open device!\n");
      fflush(stdout);
      exit(1);
    }
  printf("Device opened!\n");
  fflush(stdout);
  uint32_t id = ioctl(plik, MINER_IOC_READ_ID, NULL);
  printf("%lx\n",id);
  DumpHex(&id, 4);
  fflush(stdout);

  // Map data buffer
  buf = (uint32_t *) mmap(0,0x1000,PROT_READ | PROT_WRITE,MAP_SHARED,
	 plik,0x0);
  if(buf == (void *) -1l)
    {
      perror("Can't map data buffer!\n");
      fflush(stdout);
      exit(1);
    }

  uint64_t block_data[128] = { 0 };
  uint64_t block_local_old[128] = { 0 };
  uint64_t block_local_new[128] = { 0 };
  
  // Generate input vectors
  for(int i=0; i<128; ++i)
  {
    // uint64_t x = ((6*(uint64_t)i)<<32) | ((6*(uint64_t)i)+3); // Good debug case
    uint64_t x = ( ((uint64_t)get_random32()) << 32 ) | ((uint64_t)get_random32());
    block_data[i] = x;
    block_local_old[i] = x;
    block_local_new[i] = x;
  }
  
  
  // Begin test
  printf("In:  copying block_data:%p to buf:%p\n", (void*) block_data, (void*) buf);
  memcpy((void*)buf, block_data, 1024);
  printf("OrigIn:\n");
  DumpHex((void*)block_local_old, 1024);
  printf("FPGAIn:\n");
  DumpHex((void*)buf, 1024);
  printf("Resetting FPGA\n");
  fflush(stdout);
  ioctl(plik, MINER_IOC_WRITE_CTRL, 2);
  printf("Status after reset: %lu\n",ioctl(plik, MINER_IOC_READ_STATUS, NULL));
  printf("Ctrl before setting 'start' to 0: %lu\n",ioctl(plik, MINER_IOC_READ_CTRL, NULL));
  //Idle
  usleep(1000);
  ioctl(plik, MINER_IOC_WRITE_CTRL, 0);
  printf("Ctrl after setting 'start' to 0: %lu\n",ioctl(plik, MINER_IOC_READ_CTRL, NULL));
  ioctl(plik, MINER_IOC_WRITE_ADDR, NULL);
  ioctl(plik, MINER_IOC_WRITE_LEN, 256);
  //Start
  ioctl(plik, MINER_IOC_WRITE_CTRL, 1);
  printf("Start\n");
  fflush(stdout);
  
  // Apply Blake2 on columns of 64-bit words: (0,1,...,15) , then (16,17,..31)... finally (112,113,...127)
  for (unsigned i = 0; i < 8; ++i) {
    BLAKE2_ROUND_NOMSG(block_local_new[16 * i],      block_local_new[16 * i + 1],  block_local_new[16 * i + 2],  block_local_new[16 * i + 3],
                       block_local_new[16 * i + 4],  block_local_new[16 * i + 5],  block_local_new[16 * i + 6],  block_local_new[16 * i + 7],
                       block_local_new[16 * i + 8],  block_local_new[16 * i + 9],  block_local_new[16 * i + 10], block_local_new[16 * i + 11],
                       block_local_new[16 * i + 12], block_local_new[16 * i + 13], block_local_new[16 * i + 14], block_local_new[16 * i + 15]);
   }
  // Apply Blake2 on rows of 64-bit words: (0,1,16,17,...112,113), then (2,3,18,19,...,114,115).. finally (14,15,30,31,...,126,127)
  for (unsigned i = 0; i < 8; i++) {
    BLAKE2_ROUND_NOMSG(block_local_new[2 * i],      block_local_new[2 * i + 1],  block_local_new[2 * i + 16],  block_local_new[2 * i + 17],
                       block_local_new[2 * i + 32], block_local_new[2 * i + 33], block_local_new[2 * i + 48],  block_local_new[2 * i + 49],
                       block_local_new[2 * i + 64], block_local_new[2 * i + 65], block_local_new[2 * i + 80],  block_local_new[2 * i + 81],
                       block_local_new[2 * i + 96], block_local_new[2 * i + 97], block_local_new[2 * i + 112], block_local_new[2 * i + 113]);
  }
  
  int cnt = -1;
  while(1) // Primitive wait - TODO: Replace with irq
  {
      if(++cnt < 1) { // Print every ~3s - waiting shouldn't be that long!
        printf("Waiting for ready status\n");
        fflush(stdout);
        if(cnt = 3000) cnt = -1;
      }
      uint32_t status = ioctl(plik, MINER_IOC_READ_STATUS, NULL);
      printf("Status: %lu\n",status);
      if(status) break;
      usleep(1000); // Remove for profiling
  }
  printf("Out: copying buf:%p to block_data:%p\n", (void*) buf, (void*) block_data);
  memcpy(block_data, (void*)buf, 1024);
  
  uint8_t pass_flag = 1;
  for(int i=0; i<128; ++i)
  {
    if(block_local_new[i] != block_data[i]) {
      pass_flag=0;
      printf("i = %d\n", i);
      printf("%8.8" PRIX64 " != %8.8" PRIX64 "\n", block_local_new[i], block_data[i]);
      fflush(stdout);
      break;
    }
    printf("%8.8" PRIX64 " == %8.8" PRIX64 "\n", block_local_new[i], block_data[i]);
    fflush(stdout);
  }
  
  ioctl(plik, MINER_IOC_WRITE_CTRL, 0);
  printf("Ctrl after setting 'start' to 0: %lu\nEnd of test.\n",ioctl(plik, MINER_IOC_READ_CTRL, NULL));
  fflush(stdout);
  
  printf("OrigOut:\n");
  fflush(stdout);
  DumpHex(block_local_new, 1024);
  printf("FPGAOut:\n");
  DumpHex(block_data, 1024);

  if(pass_flag){
    printf("TEST PASSED\n");
  } else {
    printf("TEST FAILED\n");
  }
  fflush(stdout);
  
  close(plik);
  
  exit(EXIT_SUCCESS);
}
