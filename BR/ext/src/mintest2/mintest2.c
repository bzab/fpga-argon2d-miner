#include<stdio.h>
#include<sys/types.h>
#include<stdint.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<inttypes.h>

#include<sys/ioctl.h>
#include<sys/time.h>

#include "miner_ioc_cmds.h"

#define rotr64(x, y)  (((x) >> (y)) ^ ((x) << (64 - (y))))

#define G(a,b,c,d) \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 32); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 24); \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 16); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 63); 

#define BLAKE2_ROUND_NOMSG(v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15)  \
	G(v0, v4, v8, v12); \
	G(v1, v5, v9, v13); \
	G(v2, v6, v10, v14); \
	G(v3, v7, v11, v15); \
	G(v0, v5, v10, v15); \
	G(v1, v6, v11, v12); \
	G(v2, v7, v8, v13); \
	G(v3, v4, v9, v14); 

int fminer;

/*designed by the Lyra PHC team */
static inline uint64_t fBlaMka(uint64_t x, uint64_t y)
{
	uint32_t lessX = (uint32_t)x;
	uint32_t lessY = (uint32_t)y;

	uint64_t lessZ = (uint64_t)lessX;
	lessZ = lessZ * lessY;
	lessZ = lessZ << 1;

	uint64_t z = lessZ + x + y;

	return z;
}

void test_arm(volatile uint64_t* block_orig, unsigned long long n)
{
    uint64_t block[128];
    uint64_t zero_block[128] = { 0 };
    unsigned long long j;
    
    long start, end;
    struct timeval timecheck;

    gettimeofday(&timecheck, NULL);
    start = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
    
    for(j=n; j--;)
    {
        for (unsigned i = 0; i < 128; ++i) {
            block[i] = block_orig[i] ^ zero_block[i];
        }
        // Apply Blake2 on columns of 64-bit words: (0,1,...,15) , then (16,17,..31)... finally (112,113,...127)
        for (unsigned i = 0; i < 8; ++i) {
            BLAKE2_ROUND_NOMSG(block[16 * i], block[16 * i + 1], block[16 * i + 2], block[16 * i + 3],
                    block[16 * i + 4], block[16 * i + 5], block[16 * i + 6], block[16 * i + 7],
                    block[16 * i + 8], block[16 * i + 9], block[16 * i + 10], block[16 * i + 11],
                    block[16 * i + 12], block[16 * i + 13], block[16 * i + 14], block[16 * i + 15]);
        }
        // Apply Blake2 on rows of 64-bit words: (0,1,16,17,...112,113), then (2,3,18,19,...,114,115).. finally (14,15,30,31,...,126,127)
        for (unsigned i = 0; i < 8; i++) {
            BLAKE2_ROUND_NOMSG(block[2 * i], block[2 * i + 1], block[2 * i + 16], block[2 * i + 17],
                    block[2 * i + 32], block[2 * i + 33], block[2 * i + 48], block[2 * i + 49],
                    block[2 * i + 64], block[2 * i + 65], block[2 * i + 80], block[2 * i + 81],
                    block[2 * i + 96], block[2 * i + 97], block[2 * i + 112], block[2 * i + 113]);
        }
        for (unsigned i = 0; i < 128; ++i) {
            block_orig[i] ^= block[i];
        }
        block_orig[0] += 1;
    }
    
    gettimeofday(&timecheck, NULL);
    end = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
    
    FILE *fptr;
    fptr = fopen("time2.log", "a");

    // exiting program 
    if (fptr == NULL) {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "Mix ARM runs: %llu time: %ld ms\n", n, (end - start));
    fclose(fptr);
}

void test_fpga(uint64_t* block, unsigned long long n, volatile uint64_t * buf)
{
    uint64_t zero_block[128] = { 0 };
    unsigned long long j;
    
    long start, end;
    struct timeval timecheck;

    gettimeofday(&timecheck, NULL);
    start = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
    
    for(j=n; j--;)
    {
        unsigned int i; 
        for(i = 128; i--; ){
            buf[i] = block[i] ^ zero_block[i];
        }
        ioctl(fminer, MINER_IOC_WRITE_CTRL, 1); // Start coproc
        while(!ioctl(fminer, MINER_IOC_READ_STATUS, NULL)){ // Wait for coprocessor
            //usleep(1);
        }        
        
        for(i = 128; i--; ){
            block[i] ^= buf[i];
        }
        ioctl(fminer, MINER_IOC_WRITE_CTRL, 0); // Stop coproc
        block[0] += 1;
    }
    
    gettimeofday(&timecheck, NULL);
    end = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
    
    FILE *fptr;
    fptr = fopen("time2.log", "a");

    // exiting program 
    if (fptr == NULL) {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr, "Mix FPGA runs: %llu time: %ld ms\n", n, (end - start));
    fclose(fptr);
}

uint32_t get_random32()
{
    uint32_t x;
    x = rand() & 0xff;
    x |= (rand() & 0xff) << 8;
    x |= (rand() & 0xff) << 16;
    x |= (rand() & 0xff) << 24;

    return x;
}

int main(int argc, char **argv)
{
    unsigned long long n = 0;
    volatile uint64_t * buf;
    
    for (unsigned int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-help") == 0) {
            printf("=================================================\n");
            printf("Argon2 MIX - test comparsion of 2 implementations \n");
            printf("==================================================\n");
            printf("\t -help\n");
            printf("\t -r < Number of runs > <= 1000000000\n"); 
            return 0;
        }
        
        if (strcmp(argv[i], "-r") == 0) {
            char* end;
            n = strtoull(argv[i+1],&end,10) % 1000000001;
            if(*end) {
                n = 0;
                printf("Incorrect -runs number\n");
            } else {
                printf("Runs: %llu \n",n);
            }
            continue;
        }
    }
    
    fminer=-1;
    fminer=open("/dev/miner0", O_RDWR);
    if(fminer==-1)
    {
        perror("/dev/miner0");
        printf("I can't open device!\n");
        fflush(stdout);
        exit(1);
    }

    // Map data buffer
    buf = (uint64_t *) mmap(0,0x1000,PROT_READ | PROT_WRITE,MAP_SHARED,
        fminer,0x0);
    if(buf == (void *) -1l)
    {
        perror("Can't map data buffer!\n");
        fflush(stdout);
        exit(1);
    }

    ioctl(fminer, MINER_IOC_WRITE_CTRL, 2);
    usleep(1000);
    ioctl(fminer, MINER_IOC_WRITE_CTRL, 0);
    ioctl(fminer, MINER_IOC_WRITE_ADDR, NULL);
    ioctl(fminer, MINER_IOC_WRITE_LEN, 256);   
    // Coproc initialized

    volatile uint64_t block_arm[128] = { 0 }; // Volatile to not to let compiler optimize out memcpys
    uint64_t block_fpga[128] = { 0 };

    // Generate input vectors
    for(unsigned int i=0; i<128; ++i)
    {
        uint64_t x = ( ((uint64_t)get_random32()) << 32 ) | ((uint64_t)get_random32());
        block_arm[i] = x;
        block_fpga[i] = x;
    }

    test_arm(block_arm, n);
    test_fpga(block_fpga, n, buf);
    
    for(unsigned int i=128; i--;)
    {
        if(block_arm[i] ^ block_fpga[i]) {
            printf("Result blocks aren't equal!\n");
            break;
        }
    }
    
    return 0;
}
