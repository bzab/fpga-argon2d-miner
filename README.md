Quartus project:
----------------

After opening the *.qpf* file, Quartus will report few warnings about missing *.qip* files.

In case that happens:
1. Open  Platform Designer
2. Load *.qsys* file
3. Generate HDL (vhdl, default paths)
4. Close and re-open project in Quartus

Buildroot:
----------

Currently used version: 2018.08.2

CPUMiner-Multi:
---------------

Check /cpuminersrc/README.md