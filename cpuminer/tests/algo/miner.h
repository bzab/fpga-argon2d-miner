#ifndef __MINER_H__
#define __MINER_H__

#include <inttypes.h>

static inline void be32enc(void *pp, uint32_t x)
{
	uint8_t *p = (uint8_t *)pp;
	p[3] = x & 0xff;
	p[2] = (x >> 8) & 0xff;
	p[1] = (x >> 16) & 0xff;
	p[0] = (x >> 24) & 0xff;
}

void argon2d_crds_hash(void *output, const void *input);
void argon2d_dyn_hash(void *output, const void *input);

#endif /* __MINER_H__ */
