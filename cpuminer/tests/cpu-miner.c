#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>


#include<sys/types.h>
#include<stdint.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>
#include <unistd.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include <sys/ioctl.h>
#include "compat.h"
#include "algo/miner.h"



#include "algo/miner_ioc_cmds.h"


int plik = -1;
volatile uint32_t * bzbuf;

int main(int argc, char *argv[])
{
  printf("ver 004\n");
  printf("I'm trying to open our device!\n");
  fflush(stdout);
  plik=-1;
  plik=open("/dev/miner0", O_RDWR);
  if(plik==-1)
    {
      perror("/dev/miner0");
      printf("I can't open device!\n");
      //fflush(stdout);
      //exit(1);
    }
  printf("Device opened!\n");
  fflush(stdout);
  long id = ioctl(plik, MINER_IOC_READ_ID, NULL);
  printf("%lx\n",id);
  //printf("Ctrl:%lu\n", ioctl(plik, MINER_IOC_READ_CTRL, NULL));
  //printf("Status before reset: %lu\n",ioctl(plik, MINER_IOC_READ_STATUS, NULL));
  fflush(stdout);

  //Map data buffer
  bzbuf = (uint32_t *) mmap(0,0x1000,PROT_READ | PROT_WRITE,MAP_SHARED,
	 plik,0x0);
  if(bzbuf == (void *) -1l)
    {
      perror("I can't map data buffer!\n");
    }
	
	uint8_t endiandata[80];
	uint8_t hash[32];
	  for(int x = 0; x < 80; x++)
        endiandata[x] = 0x01;
	
  
  for(int x = 0; x < 100; x++)
	  argon2d_crds_hash(hash, endiandata);
	
  printf("hash:\n");
  for(int x = 0; x < 32; x++)
        printf("%02x", hash[x]);

	return 0;
}
