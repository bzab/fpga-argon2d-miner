library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity miner is

  port (
    clock_sink_clk   : in std_logic := '0';
    reset_sink_reset : in std_logic := '0';

    avalon_master_write         : out std_logic;
    avalon_master_read          : out std_logic;
    avalon_master_readdata      : in  std_logic_vector(31 downto 0) := (others => '0');
    avalon_master_readdatavalid : in  std_logic                     := '0';
    avalon_master_writedata     : out std_logic_vector(31 downto 0);
    avalon_master_address       : out std_logic_vector(31 downto 0);
    avalon_master_waitrequest   : in  std_logic                     := '0';
    avalon_master_response      : in  std_logic_vector(1 downto 0)  := (others => '0');
    avalon_master_burstcount    : out std_logic_vector(27 downto 0);

    avalon_slave_address     : in  std_logic_vector(2 downto 0)  := (others => '0');
    avalon_slave_read        : in  std_logic                     := '0';
    avalon_slave_readdata    : out std_logic_vector(31 downto 0);
    avalon_slave_waitrequest : out std_logic                     := '1';
    avalon_slave_write       : in  std_logic                     := '0';
    avalon_slave_writedata   : in  std_logic_vector(31 downto 0) := (others => '0');
    avalon_slave_response    : out std_logic_vector(1 downto 0)
    );

end entity miner;

architecture df of miner is

  component comm is
    port (
      clock_sink_clk   : in  std_logic := '0';
      reset_sink_reset : in  std_logic := '0';
      internal_reset   : out std_logic := '0';

      avalon_master_write         : out std_logic;
      avalon_master_read          : out std_logic;
      avalon_master_readdata      : in  std_logic_vector(31 downto 0) := (others => '0');
      avalon_master_readdatavalid : in  std_logic                     := '0';
      avalon_master_writedata     : out std_logic_vector(31 downto 0);
      avalon_master_address       : out std_logic_vector(31 downto 0);
      avalon_master_waitrequest   : in  std_logic                     := '0';
      avalon_master_response      : in  std_logic_vector(1 downto 0)  := (others => '0');
      avalon_master_burstcount    : out std_logic_vector(27 downto 0);

      avalon_slave_address     : in  std_logic_vector(2 downto 0)  := (others => '0');
      avalon_slave_read        : in  std_logic                     := '0';
      avalon_slave_readdata    : out std_logic_vector(31 downto 0);
      avalon_slave_waitrequest : out std_logic                     := '1';
      avalon_slave_write       : in  std_logic                     := '0';
      avalon_slave_writedata   : in  std_logic_vector(31 downto 0) := (others => '0');
      avalon_slave_response    : out std_logic_vector(1 downto 0);

      data       : out std_logic_vector(31 downto 0);
      swrite     : out std_logic;
      req_data   : out std_logic;
      data_ready : in  std_logic;
      to_send    : in  std_logic_vector(31 downto 0)
      );

  end component comm;

  component mixer is
    port (
      clk   : in std_logic;
      reset : in std_logic;
      start : in std_logic;
      swait : in std_logic;
      in1   : in unsigned(63 downto 0);
      in2   : in unsigned(63 downto 0);
      in3   : in unsigned(63 downto 0);
      in4   : in unsigned(63 downto 0);

      out1   : out unsigned(63 downto 0);
      out2   : out unsigned(63 downto 0);
      out3   : out unsigned(63 downto 0);
      out4   : out unsigned(63 downto 0);
      swrite : out std_logic
      );

  end component mixer;

  component reg is
    port (
      clk         : in std_logic;
      reset       : in std_logic;
      in_data     : in std_logic_vector(31 downto 0);
      swrite_comm : in std_logic;
      req_data    : in std_logic;

      to_mix1 : out unsigned(63 downto 0);
      to_mix2 : out unsigned(63 downto 0);
      to_mix3 : out unsigned(63 downto 0);
      to_mix4 : out unsigned(63 downto 0);
      start   : out std_logic;
      swait   : out std_logic;

      from_mix1  : in unsigned(63 downto 0);
      from_mix2  : in unsigned(63 downto 0);
      from_mix3  : in unsigned(63 downto 0);
      from_mix4  : in unsigned(63 downto 0);
      swrite_mix : in std_logic;

      to_comm    : out std_logic_vector(31 downto 0);
      data_ready : out std_logic
      );

  end component reg;

  signal int_reset              : std_logic                     := '0';
  signal comm_to_reg_wrt        : std_logic                     := '0';
  signal comm_req_data          : std_logic                     := '0';
  signal reg_to_comm_data_ready : std_logic                     := '0';
  signal reg_to_mix_start       : std_logic                     := '0';
  signal reg_to_mix_swait       : std_logic                     := '0';
  signal mix_to_reg_wrt         : std_logic                     := '0';
  signal reg_to_mix_1           : unsigned(63 downto 0)         := (others => '0');
  signal reg_to_mix_2           : unsigned(63 downto 0)         := (others => '0');
  signal reg_to_mix_3           : unsigned(63 downto 0)         := (others => '0');
  signal reg_to_mix_4           : unsigned(63 downto 0)         := (others => '0');
  signal mix_to_reg_1           : unsigned(63 downto 0)         := (others => '0');
  signal mix_to_reg_2           : unsigned(63 downto 0)         := (others => '0');
  signal mix_to_reg_3           : unsigned(63 downto 0)         := (others => '0');
  signal mix_to_reg_4           : unsigned(63 downto 0)         := (others => '0');
  signal comm_to_reg            : std_logic_vector(31 downto 0) := (others => '0');
  signal reg_to_comm            : std_logic_vector(31 downto 0) := (others => '0');

begin  -- architecture df

  comm_0 : component comm
    port map (
      clock_sink_clk   => clock_sink_clk,
      reset_sink_reset => reset_sink_reset,
      internal_reset   => int_reset,

      avalon_master_write         => avalon_master_write,
      avalon_master_read          => avalon_master_read,
      avalon_master_readdata      => avalon_master_readdata,
      avalon_master_readdatavalid => avalon_master_readdatavalid,
      avalon_master_writedata     => avalon_master_writedata,
      avalon_master_address       => avalon_master_address,
      avalon_master_waitrequest   => avalon_master_waitrequest,
      avalon_master_response      => avalon_master_response,
      avalon_master_burstcount    => avalon_master_burstcount,

      avalon_slave_address     => avalon_slave_address,
      avalon_slave_read        => avalon_slave_read,
      avalon_slave_readdata    => avalon_slave_readdata,
      avalon_slave_waitrequest => avalon_slave_waitrequest,
      avalon_slave_write       => avalon_slave_write,
      avalon_slave_writedata   => avalon_slave_writedata,
      avalon_slave_response    => avalon_slave_response,

      data     => comm_to_reg,
      swrite   => comm_to_reg_wrt,
      req_data => comm_req_data,

      data_ready => reg_to_comm_data_ready,
      to_send    => reg_to_comm
      );

  r : component reg
    port map (
      clk   => clock_sink_clk,
      reset => int_reset,

      in_data     => comm_to_reg,
      swrite_comm => comm_to_reg_wrt,

      to_mix1 => reg_to_mix_1,
      to_mix2 => reg_to_mix_2,
      to_mix3 => reg_to_mix_3,
      to_mix4 => reg_to_mix_4,
      start   => reg_to_mix_start,
      swait   => reg_to_mix_swait,

      from_mix1  => mix_to_reg_1,
      from_mix2  => mix_to_reg_2,
      from_mix3  => mix_to_reg_3,
      from_mix4  => mix_to_reg_4,
      swrite_mix => mix_to_reg_wrt,

      to_comm    => reg_to_comm,
      req_data   => comm_req_data,
      data_ready => reg_to_comm_data_ready
      );

  mix : component mixer
    port map (
      clk   => clock_sink_clk,
      reset => int_reset,

      in1   => reg_to_mix_1,
      in2   => reg_to_mix_2,
      in3   => reg_to_mix_3,
      in4   => reg_to_mix_4,
      start => reg_to_mix_start,
      swait => reg_to_mix_swait,

      out1   => mix_to_reg_1,
      out2   => mix_to_reg_2,
      out3   => mix_to_reg_3,
      out4   => mix_to_reg_4,
      swrite => mix_to_reg_wrt
      );

end architecture df;
