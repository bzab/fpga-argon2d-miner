library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mixer is
  port(
    clk   : in std_logic;
    reset : in std_logic;
    start : in std_logic;
    swait : in std_logic;
    in1   : in unsigned(63 downto 0);
    in2   : in unsigned(63 downto 0);
    in3   : in unsigned(63 downto 0);
    in4   : in unsigned(63 downto 0);

    out1   : out unsigned(63 downto 0);
    out2   : out unsigned(63 downto 0);
    out3   : out unsigned(63 downto 0);
    out4   : out unsigned(63 downto 0);
    swrite : out std_logic
    );
end mixer;

architecture rtl of mixer is

  type T_DATA_REG_COL is array (3 downto 0) of unsigned(63 downto 0);
  type T_DATA_REG is array (3 downto 0) of T_DATA_REG_COL;

  type T_STATE is (ST_INIT, ST_IDLE, ST_MIX);

  type T_REGS is record
    cnt   : unsigned(4 downto 0);
    data  : T_DATA_REG;
    state : T_STATE;
  end record T_REGS;

  constant REGS_INIT : T_REGS := (
    cnt   => (others => '0'),
    data  => (others => (others => (others => '0'))),
    state => ST_INIT
    );

  signal r, r_n : T_REGS := REGS_INIT;

  alias A_A : T_DATA_REG_COL is r.data(0);
  alias A_B : T_DATA_REG_COL is r.data(1);
  alias A_C : T_DATA_REG_COL is r.data(2);
  alias A_D : T_DATA_REG_COL is r.data(3);

  alias A_A_N : T_DATA_REG_COL is r_n.data(0);
  alias A_B_N : T_DATA_REG_COL is r_n.data(1);
  alias A_C_N : T_DATA_REG_COL is r_n.data(2);
  alias A_D_N : T_DATA_REG_COL is r_n.data(3);

  type T_COMB is record
    to_reg1 : unsigned(63 downto 0);
    to_reg2 : unsigned(63 downto 0);
    to_reg3 : unsigned(63 downto 0);
    to_reg4 : unsigned(63 downto 0);
    swrite  : std_logic;
  end record T_COMB;

  constant COMB_DEFAULT : T_COMB := (
    to_reg1 => (others => '0'),
    to_reg2 => (others => '0'),
    to_reg3 => (others => '0'),
    to_reg4 => (others => '0'),
    swrite  => '0'
    );

  signal c : T_COMB := COMB_DEFAULT;

begin

  out1   <= c.to_reg1;
  out2   <= c.to_reg2;
  out3   <= c.to_reg3;
  out4   <= c.to_reg4;
  swrite <= c.swrite;

  mix_comb : process (A_A, A_B, A_C, A_D, in1, in2, in3, in4, r, start, swait) is
    variable wait_on_load : std_logic := '0';
    variable wait_on_send : std_logic := '0';

    function fBlaMka (
      signal x : unsigned(63 downto 0);
      signal y : unsigned(63 downto 0))
      return unsigned is
      variable ret : unsigned(63 downto 0) := (others => '0');
    begin  -- function fBlaMka
      ret := x + y + shift_left(x(31 downto 0) * y(31 downto 0), 1);
      return ret;
    end function fBlaMka;

    function xor_rotr64 (
      signal x   : unsigned(63 downto 0);
      signal y   : unsigned(63 downto 0);
      constant r : integer range 0 to 63)
      return unsigned is
      variable ret : unsigned(63 downto 0) := (others => '0');
    begin  -- function xor_rotr64
      ret := (x xor y) ror r;
      return ret;
    end function xor_rotr64;

  begin  -- process mix_comb
    c   <= COMB_DEFAULT;
    r_n <= r;

    wait_on_load := '0';
    wait_on_send := '0';
    if swait = '1' and r.state = ST_MIX then
      if r.cnt < 4 then
        wait_on_load := '1';
      elsif r.cnt > 15 then
        wait_on_send := '1';
      end if;
    end if;

    if not(wait_on_load = '1' or wait_on_send = '1') then
      case r.state is
        when ST_INIT =>
          r_n.state <= ST_IDLE;
        when ST_IDLE =>
          if start = '1' then
            r_n.cnt   <= (others => '0');
            r_n.state <= ST_MIX;
          end if;
        when ST_MIX =>
          r_n.cnt <= r.cnt + 1;
          case to_integer(r.cnt) is
            when 0 =>
              A_A_N(0) <= in1;
              A_A_N(1) <= in2;
              A_A_N(2) <= in3;
              A_A_N(3) <= in4;
            when 1 =>
              A_A_N(0) <= fBlaMka(A_A(0), in1);
              A_B_N(0) <= in1;
              A_A_N(1) <= fBlaMka(A_A(1), in2);
              A_B_N(1) <= in2;
              A_A_N(2) <= fBlaMka(A_A(2), in3);
              A_B_N(2) <= in3;
              A_A_N(3) <= fBlaMka(A_A(3), in4);
              A_B_N(3) <= in4;
            when 2 =>
              A_D_N(0) <= xor_rotr64(in1, A_A(0), 32);
              A_D_N(1) <= xor_rotr64(in2, A_A(1), 32);
              A_D_N(2) <= xor_rotr64(in3, A_A(2), 32);
              A_D_N(3) <= xor_rotr64(in4, A_A(3), 32);
            when 3 =>
              A_C_N(0) <= fBlaMka(A_D(0), in1);
              A_C_N(1) <= fBlaMka(A_D(1), in2);
              A_C_N(2) <= fBlaMka(A_D(2), in3);
              A_C_N(3) <= fBlaMka(A_D(3), in4);
            when 4 =>
              A_B_N(0) <= xor_rotr64(A_B(0), A_C(0), 24);
              A_B_N(1) <= xor_rotr64(A_B(1), A_C(1), 24);
              A_B_N(2) <= xor_rotr64(A_B(2), A_C(2), 24);
              A_B_N(3) <= xor_rotr64(A_B(3), A_C(3), 24);
            when 5 =>
              A_A_N(0) <= fBlaMka(A_A(0), A_B(0));
              A_A_N(1) <= fBlaMka(A_A(1), A_B(1));
              A_A_N(2) <= fBlaMka(A_A(2), A_B(2));
              A_A_N(3) <= fBlaMka(A_A(3), A_B(3));
            when 6 =>
              A_D_N(0) <= xor_rotr64(A_D(0), A_A(0), 16);
              A_D_N(1) <= xor_rotr64(A_D(1), A_A(1), 16);
              A_D_N(2) <= xor_rotr64(A_D(2), A_A(2), 16);
              A_D_N(3) <= xor_rotr64(A_D(3), A_A(3), 16);
            when 7 =>
              A_C_N(0) <= fBlaMka(A_C(0), A_D(0));
              A_C_N(1) <= fBlaMka(A_C(1), A_D(1));
              A_C_N(2) <= fBlaMka(A_C(2), A_D(2));
              A_C_N(3) <= fBlaMka(A_C(3), A_D(3));
            when 8 =>
              A_B_N(0) <= xor_rotr64(A_B(0), A_C(0), 63);
              A_B_N(1) <= xor_rotr64(A_B(1), A_C(1), 63);
              A_B_N(2) <= xor_rotr64(A_B(2), A_C(2), 63);
              A_B_N(3) <= xor_rotr64(A_B(3), A_C(3), 63);
            when 9 =>                   -- start mixing
              A_A_N(0) <= fBlaMka(A_A(0), A_B(1));
              A_B_N(0) <= A_B(1);
              A_A_N(1) <= fBlaMka(A_A(1), A_B(2));
              A_B_N(1) <= A_B(2);
              A_A_N(2) <= fBlaMka(A_A(2), A_B(3));
              A_B_N(2) <= A_B(3);
              A_A_N(3) <= fBlaMka(A_A(3), A_B(0));
              A_B_N(3) <= A_B(0);
            when 10 =>
              A_D_N(0) <= xor_rotr64(A_D(3), A_A(0), 32);
              A_D_N(1) <= xor_rotr64(A_D(0), A_A(1), 32);
              A_D_N(2) <= xor_rotr64(A_D(1), A_A(2), 32);
              A_D_N(3) <= xor_rotr64(A_D(2), A_A(3), 32);
            when 11 =>
              A_C_N(0) <= fBlaMka(A_D(0), A_C(2));
              A_C_N(1) <= fBlaMka(A_D(1), A_C(3));
              A_C_N(2) <= fBlaMka(A_D(2), A_C(0));
              A_C_N(3) <= fBlaMka(A_D(3), A_C(1));
            when 12 =>
              A_B_N(0) <= xor_rotr64(A_B(0), A_C(0), 24);
              A_B_N(1) <= xor_rotr64(A_B(1), A_C(1), 24);
              A_B_N(2) <= xor_rotr64(A_B(2), A_C(2), 24);
              A_B_N(3) <= xor_rotr64(A_B(3), A_C(3), 24);
            when 13 =>
              A_A_N(0) <= fBlaMka(A_A(0), A_B(0));
              A_A_N(1) <= fBlaMka(A_A(1), A_B(1));
              A_A_N(2) <= fBlaMka(A_A(2), A_B(2));
              A_A_N(3) <= fBlaMka(A_A(3), A_B(3));
            when 14 =>
              A_D_N(0) <= xor_rotr64(A_D(0), A_A(0), 16);
              A_D_N(1) <= xor_rotr64(A_D(1), A_A(1), 16);
              A_D_N(2) <= xor_rotr64(A_D(2), A_A(2), 16);
              A_D_N(3) <= xor_rotr64(A_D(3), A_A(3), 16);
            when 15 =>
              A_C_N(0) <= fBlaMka(A_C(0), A_D(0));
              A_C_N(1) <= fBlaMka(A_C(1), A_D(1));
              A_C_N(2) <= fBlaMka(A_C(2), A_D(2));
              A_C_N(3) <= fBlaMka(A_C(3), A_D(3));
            when 16 =>
              c.to_reg1 <= A_A(0);
              c.to_reg2 <= A_A(1);
              c.to_reg3 <= A_A(2);
              c.to_reg4 <= A_A(3);
              c.swrite  <= '1';

              A_B_N(0) <= xor_rotr64(A_B(0), A_C(0), 63);
              A_B_N(1) <= xor_rotr64(A_B(1), A_C(1), 63);
              A_B_N(2) <= xor_rotr64(A_B(2), A_C(2), 63);
              A_B_N(3) <= xor_rotr64(A_B(3), A_C(3), 63);
            when 17 =>
              c.to_reg1 <= A_B(3);
              c.to_reg2 <= A_B(0);
              c.to_reg3 <= A_B(1);
              c.to_reg4 <= A_B(2);
              c.swrite  <= '1';
            when 18 =>
              c.to_reg1 <= A_C(2);
              c.to_reg2 <= A_C(3);
              c.to_reg3 <= A_C(0);
              c.to_reg4 <= A_C(1);
              c.swrite  <= '1';
            when 19 =>
              c.to_reg1 <= A_D(1);
              c.to_reg2 <= A_D(2);
              c.to_reg3 <= A_D(3);
              c.to_reg4 <= A_D(0);
              c.swrite  <= '1';

              r_n.state <= ST_IDLE;
            when others => null;
          end case;
        when others => null;
      end case;
    end if;  -- not(wait_on_load or wait_on_send)
  end process mix_comb;

  mix_seq : process (clk) is
  begin  -- process mix_seq
    if rising_edge(clk) then            -- rising clock edge
      if reset = '1' then               -- synchronous reset (active high)
        r <= REGS_INIT;
      else
        r <= r_n;
      end if;
    end if;
  end process mix_seq;

end rtl;
