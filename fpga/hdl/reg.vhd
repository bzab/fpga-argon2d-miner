library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reg is
  port(
    clk         : in std_logic;
    reset       : in std_logic;
    in_data     : in std_logic_vector(31 downto 0);
    swrite_comm : in std_logic;
    req_data    : in std_logic;

    to_mix1 : out unsigned(63 downto 0);
    to_mix2 : out unsigned(63 downto 0);
    to_mix3 : out unsigned(63 downto 0);
    to_mix4 : out unsigned(63 downto 0);
    start   : out std_logic;
    swait   : out std_logic;

    from_mix1  : in unsigned(63 downto 0);
    from_mix2  : in unsigned(63 downto 0);
    from_mix3  : in unsigned(63 downto 0);
    from_mix4  : in unsigned(63 downto 0);
    swrite_mix : in std_logic;

    to_comm    : out std_logic_vector(31 downto 0);
    data_ready : out std_logic
    );
end reg;

architecture rtl of reg is

  component mem is
    port (
      address_a : in  std_logic_vector(5 downto 0);
      address_b : in  std_logic_vector(5 downto 0);
      clock     : in  std_logic := '1';
      data_a    : in  std_logic_vector(127 downto 0);
      data_b    : in  std_logic_vector(127 downto 0);
      wren_a    : in  std_logic := '0';
      wren_b    : in  std_logic := '0';
      q_a       : out std_logic_vector(127 downto 0);
      q_b       : out std_logic_vector(127 downto 0)
      );

  end component mem;

  type T_STATE is (ST_INIT, ST_LISTEN, ST_ROUND2, ST_FLUSH_INIT, ST_FLUSH);

  type T_REGS is record
    slice_num       : unsigned(1 downto 0);
    cnt_words       : unsigned(7 downto 0);           -- Count words
                                                      -- exchanged with comm
    wait_next       : std_logic;                      -- Wait flag for syncing BRAM operations
    cnt_mix         : unsigned(5 downto 0);           -- Count mix returns
    cnt_mix_trigger : unsigned(2 downto 0);
    buf_in          : std_logic_vector(95 downto 0);  -- Buffer for deserializing
    state           : T_STATE;
  end record T_REGS;

  constant REGS_INIT : T_REGS := (
    slice_num       => (others => '0'),
    cnt_words       => (others => '0'),
    wait_next       => '0',
    cnt_mix         => (others => '0'),
    cnt_mix_trigger => (others => '0'),
    buf_in          => (others => '0'),
    state           => ST_INIT
    );

  signal r, r_n : T_REGS := REGS_INIT;

  type T_COMB is record
    -- Comm
    to_comm    : std_logic_vector(31 downto 0);
    data_ready : std_logic;

    -- Mix
    start : std_logic;
    swait : std_logic;

    -- Memory
    addr_a : std_logic_vector(5 downto 0);
    addr_b : std_logic_vector(5 downto 0);
    data_a : std_logic_vector(127 downto 0);
    data_b : std_logic_vector(127 downto 0);
    wren_a : std_logic;
    wren_b : std_logic;
  end record T_COMB;

  constant COMB_DEFAULT : T_COMB := (
    to_comm    => (others => '0'),
    data_ready => '0',

    start => '0',
    swait => '0',

    addr_a => (others => '0'),
    addr_b => (others => '0'),
    data_a => (others => '0'),
    data_b => (others => '0'),
    wren_a => '0',
    wren_b => '0'
    );

  signal c   : T_COMB                         := COMB_DEFAULT;
  signal q_a : std_logic_vector(127 downto 0) := (others => '0');
  signal q_b : std_logic_vector(127 downto 0) := (others => '0');

begin

  mem0 : component mem
    port map (
      address_a => c.addr_a,
      address_b => c.addr_b,
      clock     => clk,
      data_a    => c.data_a,
      data_b    => c.data_b,
      wren_a    => c.wren_a,
      wren_b    => c.wren_b,
      q_a       => q_a,
      q_b       => q_b
      );

  to_comm    <= c.to_comm;
  data_ready <= c.data_ready;

  to_mix1 <= unsigned(q_a(127 downto 64));
  to_mix2 <= unsigned(q_a(63 downto 0));
  to_mix3 <= unsigned(q_b(127 downto 64));
  to_mix4 <= unsigned(q_b(63 downto 0));
  start   <= c.start;
  swait   <= c.swait;

  reg_comb : process(from_mix1, from_mix2, from_mix3, from_mix4, in_data, q_a,
                     r, req_data, swrite_comm, swrite_mix)
    variable mix_addr  : unsigned(5 downto 0) := (others => '0');
    variable read_addr : unsigned(7 downto 0) := (others => '0');
    variable wait_flag : std_logic            := '0';  -- Freeze everything related to mix during writing word from comm
  begin  -- process reg_comb
    c   <= COMB_DEFAULT;
    r_n <= r;

    mix_addr  := (others => '0');
    read_addr := r.cnt_words;
    wait_flag := '0';

    case r.state is
      when ST_INIT =>
        r_n       <= REGS_INIT;
        r_n.state <= ST_LISTEN;
      when ST_LISTEN =>
        if swrite_comm = '1' then
          r_n.buf_in    <= r.buf_in(63 downto 0) & in_data;
          r_n.slice_num <= r.slice_num + 1;

          if r.slice_num = 3 then
            wait_flag := '1';           -- Freeze trigger for single tick
                                        -- during write from comm
            if r.cnt_mix_trigger = 0 then
              c.swait <= '1';           -- Send wait to mixer to pause it if it
                                        -- possibly outputs data at the moment
            else
              r_n.wait_next <= '1';     -- Word from input buffer will be
                                        -- written after this tick, so next
                                        -- data word for mixer will be ready
                                        -- after next tick. Prepare 'wait' to
                                        -- be raised for mixer for next tick
            end if;
            -- Argon2d specification, 3.6. Permutation P:
            -- 64bit word S_i consists of 2 concated 32bit words v_{2i} and v_{2i+1}:
            -- S_{i} = v_{2i+1} & v_{2i}
            -- So swap each pair of incoming 32b subwords
            -- And concatenate them into 128b word held in memory
            c.addr_a <= std_logic_vector(r.cnt_words(5 downto 0));
            c.data_a <= r.buf_in(63 downto 32) &  -- "2"
                        r.buf_in(95 downto 64) &  -- "1"
                        in_data &       -- "4"
                        r.buf_in(31 downto 0);    -- "3"
            c.wren_a <= '1';

            r_n.cnt_words <= r.cnt_words + 1;
            r_n.slice_num <= (others => '0');  -- Explicitely set to zero
          end if;  -- r.slice_num = 3

          if (r.cnt_words(2 downto 0) = 7) and (r.slice_num = 3) then
            r_n.cnt_mix_trigger <= to_unsigned(4, r.cnt_mix_trigger'length);  -- Start sending data row to the mixer
                                        -- in next tick
          end if;
        end if;  -- swrite_comm = '1'

        if r.wait_next = '1' then
          c.swait       <= '1';
          r_n.wait_next <= '0';
        end if;

        if not (wait_flag = '1') then
          if (r.cnt_mix_trigger > 0) then
            r_n.cnt_mix_trigger <= r.cnt_mix_trigger - 1;
            case to_integer(r.cnt_mix_trigger) is
              when 4 =>                 -- Numeruj od 5?
                mix_addr := r.cnt_mix(5 downto 3) & "000";
                c.start  <= '1';
              when 3 =>
                mix_addr := r.cnt_mix(5 downto 3) & "010";
              when 2 =>
                mix_addr := r.cnt_mix(5 downto 3) & "110";
              when 1 =>
                mix_addr := r.cnt_mix(5 downto 3) & "100";
              when others =>
                mix_addr := (others => '0');
            end case;

            c.addr_a <= std_logic_vector(mix_addr);
            c.addr_b <= std_logic_vector(mix_addr+1);
          end if;  -- (r.cnt_mix_trigger > 0)

          if swrite_mix = '1' then
            c.addr_a <= std_logic_vector(r.cnt_mix);
            c.addr_b <= std_logic_vector(r.cnt_mix + 1);
            c.data_a <= std_logic_vector(from_mix1 & from_mix2);
            c.data_b <= std_logic_vector(from_mix3 & from_mix4);
            c.wren_a <= '1';
            c.wren_b <= '1';

            r_n.cnt_mix <= r.cnt_mix + 2;
            if r.cnt_mix = 62 then
              r_n.state           <= ST_ROUND2;
              r_n.cnt_mix         <= to_unsigned(0, r.cnt_mix'length);  -- Explicitely set
                                                                        -- zero
              -- Set trigger to start second round of mixing -> first column
              r_n.cnt_mix_trigger <= to_unsigned(4, r.cnt_mix_trigger'length);
              r_n.cnt_words       <= to_unsigned(0, r.cnt_words'length);
            end if;
          end if;  -- swrite_mix = '1'
        end if;  -- not (wait_flag = '1')

      when ST_ROUND2 =>
        if r.cnt_mix(2 downto 0) = 6 then  -- Tick before end of writes from mixer
          r_n.cnt_mix_trigger <= to_unsigned(4, r.cnt_mix_trigger'length);  -- Start sending data row to the mixer
        end if;

        if r.cnt_mix_trigger > 0 then
          r_n.cnt_mix_trigger <= r.cnt_mix_trigger - 1;
          case to_integer(r.cnt_mix_trigger) is
            when 4 =>
              mix_addr := "000" & r.cnt_mix(5 downto 3);
              c.start  <= '1';
            when 3 =>
              mix_addr := "010" & r.cnt_mix(5 downto 3);
            when 2 =>
              mix_addr := "110" & r.cnt_mix(5 downto 3);
            when 1 =>
              mix_addr := "100" & r.cnt_mix(5 downto 3);
            when others => null;
          end case;

          c.addr_a <= std_logic_vector(mix_addr);
          c.addr_b <= std_logic_vector(mix_addr+8);
        end if;  -- r_n.cnt_mix_trigger > 0

        if swrite_mix = '1' then
          -- BRAM consists of 64 words of 128b length and can be seen as 8x8
          -- matrix. Address is 6 bit long, so modifying it with ror/rol 3
          -- transposes the matrix and allows to read column-wise.
          -- Address difference between 2 words in same column equals 8.
          c.addr_a <= std_logic_vector(r.cnt_mix ror 3);
          c.addr_b <= std_logic_vector((r.cnt_mix ror 3) + 8);
          c.data_a <= std_logic_vector(from_mix1 & from_mix2);
          c.data_b <= std_logic_vector(from_mix3 & from_mix4);
          c.wren_a <= '1';
          c.wren_b <= '1';

          r_n.cnt_mix <= r.cnt_mix + 2;
          if r.cnt_mix = 62 then
            r_n.state           <= ST_FLUSH_INIT;
            r_n.cnt_mix         <= to_unsigned(0, r.cnt_mix'length);
            r_n.cnt_mix_trigger <= to_unsigned(0, r.cnt_mix_trigger'length);
          end if;
        end if;  -- swrite_mix = '1'

      when ST_FLUSH_INIT =>
        c.addr_a     <= (others => '0');  -- Explicitely set addr_a to 0
        c.data_ready <= '1';

        r_n.cnt_words <= to_unsigned(0, r.cnt_words'length);
        r_n.slice_num <= to_unsigned(0, r.slice_num'length);
        r_n.state     <= ST_FLUSH;

      when ST_FLUSH =>
        if req_data = '1' then
          read_addr     := r.cnt_words + 1;
          r_n.cnt_words <= read_addr;

          if r.cnt_words = 255 then
            r_n.cnt_words <= to_unsigned(0, r.cnt_words'length);  -- Explicitely
                                        -- reset counters
            r_n.state     <= ST_LISTEN;
          end if;
        end if;  -- req_data = '1'

        c.addr_a <= std_logic_vector(read_addr(7 downto 2));
        -- Again swap 32b subwords inside 64b word:
        -- S_{i} = v_{2i+1} & v_{2i}
        -- Calculate which 32b subword of 128b word from memory should be
        -- written into output buffer:
        case to_integer(r.cnt_words(1 downto 0)) is
          when 0 =>
            c.to_comm <= q_a(95 downto 64);
          when 1 =>
            c.to_comm <= q_a(127 downto 96);
          when 2 =>
            c.to_comm <= q_a(31 downto 0);
          when 3 =>
            c.to_comm <= q_a(63 downto 32);
          when others => null;
        end case;

      when others => null;
    end case;

  end process reg_comb;

  reg_seq : process (clk) is
  begin  -- process am_seq
    if rising_edge(clk) then            -- rising clock edge
      if reset = '1' then               -- synchronous reset (active high)
        r <= REGS_INIT;
      else
        r <= r_n;
      end if;
    end if;
  end process reg_seq;


end rtl;
