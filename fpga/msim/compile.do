set comperror ""

set files {
mem.vhd
mem128.vhd
comm.vhd
comm_new.vhd
reg_ctrl.vhd
reg_ctrl_new.vhd
reg.vhd
reg_new.vhd
mixer.vhd
mixer_new.vhd
reg2_ctrl.vhd
reg2_ctrl_new.vhd
reg2.vhd
reg2_new.vhd
miner.vhd
miner_new.vhd
reg_tb.vhd
reg2_tb.vhd
mixer_tb.vhd
miner_tb.vhd}
 
set vcom_args ""

foreach f $files {
  if {$f != ""} {
      set dir "./../hdl/"
      append dir $f
      append dir " "
      append vcom_args $dir
  }
}

catch "vcom -quiet -work work $vcom_args" comperror
if {${comperror}!=""} {
  puts $comperror 
  exit -code 3
} else {
  exit -code 0
}

quit -f
