#!/bin/bash
VSIM_DIR='/opt/Intel/intelFPGA_lite/18.1/modelsim_ase/bin/'

if [ "$#" -eq 0 ]; then
    tbs=("reg_tb" "mixer_tb" "reg2_tb" "miner_tb")
else
    tbs=( "$@" )
fi

"${VSIM_DIR}vsim" -c -do compile.do
if [ $? -ne 0 ] ; then
    echo "COMPILATION FAILED"
    exit $? # 3 - Compilation Failed
fi

for tb in $tbs; do
    "${VSIM_DIR}vsim" -c -do verify.do $tb
    if grep -C1 "^# \*\* Failure: " ./transcript; then
        echo "TEST ${tb} FAILED"
        exit 2 # 2 -Test failed
    fi
done
echo "ALL TESTS PASSED"
exit 0
