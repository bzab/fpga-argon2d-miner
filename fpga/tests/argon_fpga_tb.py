#!/usr/bin/env python3

import random
import ctypes

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge
from cocotb.regression import TestFactory
from cocotb.result import TestFailure, TestSuccess
from cocotb.drivers.avalon import AvalonMaster, AvalonMemory

BLOCK_LEN_8 = 1024
BLOCK_LEN_16 = BLOCK_LEN_8 >> 1
BLOCK_LEN_32 = BLOCK_LEN_8 >> 2
BLOCK_LEN_64 = BLOCK_LEN_8 >> 3

# CONFIG:
TESTPATH = "./c_models/argon_models.so"
BLOCK_LEN = BLOCK_LEN_8
CLK_PERIOD_NS = 20
UINT_TYPE = 8 #

class MinerTB(object):
    def __init__(self, dut, memory, avl_properties={}):
        # Memory -> 32 bit words!!!
        mem_addrs = [k for (k, v) in sorted(memory.items())]
        # Check memory continuity
        DISTANCE = 1
        if any(a_diff != DISTANCE for a_diff in [a2-a1 for a1, a2 in zip(mem_addrs[:-1], mem_addrs[1:])]):
            raise ValueError("DUT memory isn't continous! Detected adrr diff: "+str(DISTANCE+max(abs(DISTANCE-a_diff) for a_diff in [a2-a1 for a1, a2 in zip(mem_addrs[:-1], mem_addrs[1:])])))
        
        self.dut = dut
        self.avalon_master = AvalonMemory(dut, "avalon_master", dut.clock_sink_clk,
                                          memory=memory, avl_properties=avl_properties)
        self.avalon_slave = AvalonMaster(dut, "avalon_slave", dut.clock_sink_clk)
        self.block_addr = mem_addrs[0]
        
        self.expected_out = {}
        
        # Prepare data
        ref_lib = ctypes.CDLL(TESTPATH) 
        #ref_lib.mix.argtypes = [ctypes.POINTER(ctypes.c_uint64)]
        ref_lib.argon2d_fpga.argtypes = [ctypes.POINTER(ctypes.c_uint8)]

        # Convert memory dict to list of values (sorted by address)
        data_in = [v for (k, v) in sorted(memory.items())]  
        data_arr = (ctypes.c_uint8 * len(data_in))(*data_in)
        ret = ref_lib.argon2d_fpga(data_arr) # Call c function modifying array
        # Save expected to dict similiar to memory
        if ret!=0:
            raise #TODO
        res_ptr = ctypes.cast(data_arr, ctypes.POINTER(ctypes.c_uint8))
        data_exp = [res_ptr[i] for i in range(BLOCK_LEN_8)] 
        for i, k in enumerate(sorted(memory.keys())):
            self.expected_out[k] = data_exp[i]
        
    
    async def reset_dut(self):
        await self.avalon_slave.write(1, 2)
        self.dut._log.debug("Internal reset!")
        await RisingEdge(self.dut.clock_sink_clk)
        ctrl_reg = await self.avalon_slave.read(1)
        self.dut._log.debug("Ctrl reg after reset: "+str(ctrl_reg))
        if ctrl_reg != 0:
            raise TestFailure("DUT didn't return to IDLE after reset")
    
    async def start_dut(self):
        await self.avalon_slave.write(1, 1)
        self.dut._log.debug("Write '1' to ctrl reg // Start")
        
    async def stop_dut(self):
        await self.avalon_slave.write(1, 0)
        self.dut._log.debug("Write '0' to ctrl reg // Idle")
   
    async def rd_ctrl(self):
        val = await self.avalon_slave.read(1)
        self.dut._log.debug("Ctrl reg = "+str(val))
        return val
    
    async def rd_status(self, mute_log=False):
        val = await self.avalon_slave.read(2)
        if not mute_log:
            self.dut._log.debug("Status = "+str(val))
        return val
        
    async def rd_addr(self):
        val = await self.avalon_slave.read(3)
        self.dut._log.debug("Address = "+hex(val))
        return val
        
    async def wrt_addr(self, addr):
        await self.avalon_slave.write(3, self.block_addr)
        self.dut._log.debug("Write "+hex(addr)+" to addr reg")
        
    async def rd_len(self):
        val = await self.avalon_slave.read(4)
        self.dut._log.debug("Len = "+str(val))
        return val
    
    async def wrt_len(self, l):
        await self.avalon_slave.write(4, l)
        self.dut._log.debug("Write "+str(l)+" to len reg")
    
    async def run_config(self):
        await self.reset_dut()
        await self.wrt_addr(self.block_addr)
        await self.wrt_len(BLOCK_LEN_32)
        # Add test: read ID, add script generating ID in all project files!
        # Add test reads of len / addr
        # Add tests with some wrong write/reads and test for slave respnse
        # Current CocoTB implementation ignores slave_response!
        # Current CocoTB implementation doesn't provide random pauses during read from memory
        
        # Investigate case when CocoTB hangs sometimes after receiving incorrect
        # result data -> in next run sometimes memory doesn't send last 8 words! 
        # (Typically happenned on test #82)
    
    async def data_test(self):
        await self.run_config()
        await self.start_dut()
        # Wait for results
        mute_log_flag = False
        while not await self.rd_status(mute_log_flag):
            if not mute_log_flag: # Debug log only once
                mute_log_flag = True
            await RisingEdge(self.dut.clock_sink_clk)
        
        # Compare results
        if self.expected_out != self.avalon_master._mem:
            if len(self.avalon_master._mem) != BLOCK_LEN:
                raise TestFailure("Results were written to wrong memory address")
            # Add leading zeroes to shorter values
            rcv = ['0'*(2-len(hex(self.avalon_master._mem[a])[2:]))+hex(self.avalon_master._mem[a])[2:] for a in self.avalon_master._mem]
            exp = ['0'*(2-len(hex(self.expected_out[a])[2:]))+hex(self.expected_out[a])[2:] for a in self.expected_out]
            if len(rcv) != len(exp):
                raise TestFailure("len rcv != len exp \n exp:"+str(len(exp))+" rcv:"+str(len(rcv)))
            rcv32 = []
            exp32 = []
            for i in range(len(rcv)//4):
                rcv32.append('0x'+rcv[4*i]+rcv[4*i+1]+rcv[4*i+2]+rcv[4*i+3])
                exp32.append('0x'+exp[4*i]+exp[4*i+1]+exp[4*i+2]+exp[4*i+3])
            comp_lines = ["R: "+str(rcv32[i])+" E: "+str(exp32[i]) for i in range(len(rcv32))]
            str_comp = ""
            for line in comp_lines:
                str_comp += line+"\n"
            
            raise TestFailure(str_comp+"Received result differs from expected")

        self.dut._log.info("Results OK")
        await self.stop_dut()
        if await self.rd_ctrl() != 0:
            raise TestFailure("Failed to set DUT to IDLE")
        raise TestSuccess("OK")
          
    
# Leaving block / block_addr set None generates random data   
async def run_test(dut, block=None, block_addr=None, wait_req=True, wait_req_max_len=4):
    """Setup testbench and run a test."""

    # Block consists of 8192 bits of data
    # That is 256 uint32 / 1024 uint8 words passed as list of ints
    if not block:
        block = [random.randrange(1<<UINT_TYPE) for _ in range(BLOCK_LEN)]
    if not block_addr:
        block_addr = random.randrange((2**32-BLOCK_LEN_8)//UINT_TYPE)*UINT_TYPE
    if ((not isinstance(block, list)) or (any(not isinstance(x, int) for x in block))):         
        raise TypeError("block should be a list of "+str(BLOCK_LEN)+" uint"+str(UINT_TYPE)+"s")
    if any((x<0) or (x>=1<<UINT_TYPE) for x in block):
        raise ValueError("Values inside block should be in uint"+str(UINT_TYPE)+" range\nMin: "+str(min(block))+" Max: "+str(max(block)))

    if len(block) != BLOCK_LEN:
        raise ValueError("Wrong number of block elements, expected:"+str(BLOCK_LEN)+" received:"+str(len(block)))
        
    if not isinstance(block_addr, int):
        raise TypeError("block_addr should be an int, received "
            +type(block_addr).__name__)
    
    if (block_addr < 0) or (block_addr >= (2**32-BLOCK_LEN_8)):
        raise ValueError("block_addr value should be between 0x0000_0000 and 0xFFFF_FBFF, received "+str(block_addr))
    
    # Convert block to memory dict
    memory = {i:v for i,v in enumerate(block)}
    
    avl_properties = {}
    if wait_req:
        avl_properties = {
            "WriteBurstWaitReq": True,
            "MaxWaitReqLen": wait_req_max_len,        
        }
            
    cocotb.fork(Clock(dut.clock_sink_clk, CLK_PERIOD_NS, 'ns').start(start_high=True))

    tb = MinerTB(dut, memory, avl_properties)

    clkedge = RisingEdge(dut.clock_sink_clk)
    
    # Start test
    await clkedge
    await clkedge
    dut.reset_sink_reset <= 1
    await clkedge
    dut.reset_sink_reset <= 0
    await tb.data_test()
    
    raise TestSuccess("OK")


random.seed()
# Register the test.
factory = TestFactory(run_test)
factory.add_option('block',[
                    [0 for _ in range(BLOCK_LEN_8)],
                    [0xFF for _ in range(BLOCK_LEN_8)],
                    [i//4 if i%4==0 else 0 for i in range(BLOCK_LEN_8)],
                    [i % (1<<UINT_TYPE) for i in range(BLOCK_LEN_8)],
                    None, # None -> random numbers generated during init
                    None,
                    None            
                    ])
factory.add_option('block_addr',[0, 2**32-BLOCK_LEN_8-1, None, None, None])
factory.add_option('wait_req',[False,True, True, True])
factory.add_option('wait_req_max_len',[4,random.randrange(10),random.randrange(16,32), random.randrange(32)])
factory.generate_tests()

