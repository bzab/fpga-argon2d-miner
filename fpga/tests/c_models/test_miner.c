#include<stdint.h>

#define rotr64(x, y)  (((x) >> (y)) ^ ((x) << (64 - (y))))

#define G(a,b,c,d) \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 32); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 24); \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 16); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 63); 

#define BLAKE2_ROUND_NOMSG(v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15)  \
	G(v0, v4, v8, v12); \
	G(v1, v5, v9, v13); \
	G(v2, v6, v10, v14); \
	G(v3, v7, v11, v15); \
	G(v0, v5, v10, v15); \
	G(v1, v6, v11, v12); \
	G(v2, v7, v8, v13); \
	G(v3, v4, v9, v14); 

/*designed by the Lyra PHC team */
static inline uint64_t fBlaMka(uint64_t x, uint64_t y)
{
	uint32_t lessX = (uint32_t)x;
	uint32_t lessY = (uint32_t)y;

	uint64_t lessZ = (uint64_t)lessX;
	lessZ = lessZ * lessY;
	lessZ = lessZ << 1;

	uint64_t z = lessZ + x + y;

	return z;
}

int argon2d_fpga(uint64_t *block)
{ 
  // Apply Blake2 on columns of 64-bit words: (0,1,...,15) , then (16,17,..31)... finally (112,113,...127)
  for (unsigned i = 0; i < 8; ++i) {
    BLAKE2_ROUND_NOMSG(block[16 * i], block[16 * i + 1], block[16 * i + 2], block[16 * i + 3],
                       block[16 * i + 4], block[16 * i + 5], block[16 * i + 6], block[16 * i + 7],
                       block[16 * i + 8], block[16 * i + 9], block[16 * i + 10], block[16 * i + 11],
                       block[16 * i + 12], block[16 * i + 13], block[16 * i + 14], block[16 * i + 15]);
   }
  // Apply Blake2 on rows of 64-bit words: (0,1,16,17,...112,113), then (2,3,18,19,...,114,115).. finally (14,15,30,31,...,126,127)
  for (unsigned i = 0; i < 8; i++) {
    BLAKE2_ROUND_NOMSG(block[2 * i], block[2 * i + 1], block[2 * i + 16], block[2 * i + 17],
                       block[2 * i + 32], block[2 * i + 33], block[2 * i + 48], block[2 * i + 49],
                       block[2 * i + 64], block[2 * i + 65], block[2 * i + 80], block[2 * i + 81],
                       block[2 * i + 96], block[2 * i + 97], block[2 * i + 112], block[2 * i + 113]);
  }
  
  return 0;
}
