#include <stdint.h>

#define rotr64(x, y)  (((x) >> (y)) ^ ((x) << (64 - (y))))

#define G(a,b,c,d) \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 32); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 24); \
	a = fBlaMka(a, b) ; \
	d = rotr64(d ^ a, 16); \
	c = fBlaMka(c, d); \
	b = rotr64(b ^ c, 63); 

#define BLAKE2_ROUND_NOMSG(v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15)  \
	G(v0, v4, v8, v12); \
	G(v1, v5, v9, v13); \
	G(v2, v6, v10, v14); \
	G(v3, v7, v11, v15); \
	G(v0, v5, v10, v15); \
	G(v1, v6, v11, v12); \
	G(v2, v7, v8, v13); \
	G(v3, v4, v9, v14); 

/*designed by the Lyra PHC team */
static inline uint64_t fBlaMka(uint64_t x, uint64_t y)
{
	uint32_t lessX = (uint32_t)x;
	uint32_t lessY = (uint32_t)y;

	uint64_t lessZ = (uint64_t)lessX;
	lessZ = lessZ * lessY;
	lessZ = lessZ << 1;

	uint64_t z = lessZ + x + y;

	return z;
}

void mix(uint64_t *block)
{  
  // Apply Blake2 on columns of 64-bit words: (0,1,...,15) , then (16,17,..31)... finally (112,113,...127)
  BLAKE2_ROUND_NOMSG(block[0 ], block[1 ], block[2 ], block[3 ],
                     block[4 ], block[5 ], block[6 ], block[7 ],
                     block[8 ], block[9 ], block[10], block[11],
                     block[12], block[13], block[14], block[15]);
  return;
}
