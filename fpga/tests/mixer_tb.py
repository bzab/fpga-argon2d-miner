import random
import ctypes

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge
from cocotb.monitors import Monitor
from cocotb.drivers import BitDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure, TestSuccess

TESTPATH = "./c_models/argon_models.so"
DATA_IN_LEN = 16
TICK_LIMIT = 32

class MixerTB(object):
    def __init__(self, dut):
        self.dut = dut
        self.ticks = 0
    
async def run_test(dut, data_in, interface_in=1):
    """Setup testbench and run a test."""

    # Single row sent to mixer consists of 1024 bits
    # That is 16 uint64 words passed as list of ints
    if ((not isinstance(data_in, list)) 
         or (any(not isinstance(x, int) for x in data_in))):  
        
        raise TypeError("data_in should be a list of "+str(DATA_IN_LEN)+" ints")
    
    if len(data_in) != DATA_IN_LEN:
        raise ValueError("Wrong number of data_in elements, \
        expected:"+str(DATA_IN_LEN)+" received:"+str(len(data_in)))
    if interface_in != 2:
        interface_in = 1
        
    # Ugly workaround for easier access to signals
    if interface_in == 1:
        dut_start = dut.start1
        dut_ins = [
            dut.in11,
            dut.in12,
            dut.in13,
            dut.in14,
        ]
    else:
        dut_start = dut.start2
        dut_ins = [
            dut.in21,
            dut.in22,
            dut.in23,
            dut.in24,
        ]
    
    
    dut_outs = [
        dut.out1,
        dut.out2,
        dut.out3,
        dut.out4,
    ]

    # Prepare data
    ref_lib = ctypes.CDLL(TESTPATH) 
    ref_lib.mix.argtypes = [ctypes.POINTER(ctypes.c_uint64)]

    data_arr = (ctypes.c_uint64 * len(data_in))(*data_in)
    ref_lib.mix(data_arr) # Call c function modifying array
    expected_out = list(data_arr)
    received_out = [None]*16
        
    cocotb.fork(Clock(dut.clk, 2, 'us').start(start_high=False))

    tb = MixerTB(dut)

    clkedge = RisingEdge(dut.clk)
    
    # Start test
    await clkedge
    tb.ticks += 1
    dut_start <= 1
    await clkedge
    tb.ticks += 1
    dut_start <= 0
    # Send A1-A4
    for i in range(4):
        dut_ins[i] <= data_in[i]
    await clkedge
    tb.ticks += 1
    # Send B1-B4
    for i in range(4):
        dut_ins[i] <= data_in[4+i]
    await clkedge
    tb.ticks += 1
    # Send D1-D4
    for i in range(4):
        dut_ins[i] <= data_in[12+i]
    await clkedge
    tb.ticks += 1
    # Send C1-C4
    for i in range(4):
        dut_ins[i] <= data_in[8+i]
        
    await clkedge
    tb.ticks += 1
    
    # Wait for results
    time_exceeded = False
    while not dut.swrite.value:
        if (not time_exceeded) and (tb.ticks > TICK_LIMIT):
            dut._log.error("DUT didn't return results within time limit")
            time_exceeded = True
        if tb.ticks > 3*TICK_LIMIT:
            raise TestFailure(
                "DUT didn't return results within 3 * time limit")
        await clkedge
        tb.ticks += 1
    
    # Mixer starts sending output data
    # 'swrite' should be high 
    swrite_cnt = 0
    
    for i in range(4):
        if dut.swrite.value:
            swrite_cnt+=1
        else:
            dut._log.warning("swrite wasn't continous") #TODO: Dac sensowny warning
            
        for j in range(4):
            received_out[4*i+j] = int(dut_outs[j]) # Nie dziala .integer
        await clkedge
        tb.ticks += 1        

    # Compare results
    if received_out == expected_out:
        if not time_exceeded:
            raise TestSuccess("OK")
        else:
            raise TestError("Data OK; Time limit exceeded")
    else:
        strrcv = str([hex(x) for x in received_out])[1:-1]
        strexp = str([hex(x) for x in expected_out])[1:-1]
        dut._log.info("Expected:\n"+strexp+"\n"+
                      "Received:\n"+strrcv)
        raise TestFailure("Received data differs from expected")


random.seed()
# Register the test.
factory = TestFactory(run_test)
factory.add_option('data_in',[
                    [((2*i)<<32)+(2*i+1) for i in range(16)],
                    [random.randrange(1<<64) for _ in range(16)],                    
                    ])
factory.add_option('interface_in',[1,2])
factory.generate_tests()
