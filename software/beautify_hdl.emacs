#!/usr/bin/emacs --script

(setq require-final-newline 'visit)

(defun indent-files (files)
  (cond (files
         (find-file (car files))
         (vhdl-beautify-buffer)
         (save-buffer)
         (kill-buffer)
         (indent-files (cdr files)))))

(indent-files command-line-args-left)

;; EOF ;;
