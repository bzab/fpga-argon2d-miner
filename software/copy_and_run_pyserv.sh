#CP_LIBS=0
#CP_MINER=0
cd ./..
dir=$PWD
cd /tmp
rm -rf ./fpgaserv
mkdir -p fpgaserv
cd ./fpgaserv
cp ~/BR/buildroot-2018.08.2/output/images/zImage .
cp $dir/fpga/quartus/soc_system.rbf .
cp ~/BR/buildroot-2018.08.2/output/images/socfpga_cyclone5_de0_sockit.dtb .
#if ((CP_LIBS)); then
#    cp ~/BR/libcrypto.so.1.1 .
#    cp ~/BR/libssl.so.1.1 .
#fi
#if ((CP_MINER)); then
#    cp /tmp/fpga-argon2d-miner/cpuminer .
#fi
#mkdir -p testvs
#cp ~/FPGA/testvs/*.txt ./testvs
python3 -m http.server
